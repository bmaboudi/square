import numpy as np
#import gauss_hermit as gh
import high_fidelity as hf
import metropolis as metro
import nn_network

def find_centers(model_params):
	c1 = np.random.uniform(-0.5,0.5,2)
	r1 = np.random.uniform(0.1,0.4)

	model_params['cx'] = np.zeros(1)
	model_params['cx'][0] = c1[0]

	model_params['cy'] = np.zeros(1)
	model_params['cy'][0] = c1[1]

	model_params['r'] = np.zeros(1)
	model_params['r'][0] = r1
	return model_params

high_fid = hf.high_fidelity(num_out=200)

model_params = {}
model_params = find_centers(model_params)

#modes = np.linspace(1,9,9)
#y_observed = []
#for mode in modes:
#	model_params['mode'] = [ mode ]
high_fid.set_params(model_params)
high_fid.assemble_rhs()
high_fid.assemble_bilinear_matrix()
y_observed = high_fid.solve()
#	y_observed.append( y )

#y_observed = np.reshape(y_observed,[-1])

mu = 0.0
sigma = 0.05

mtr = metro.Metropolis(y_observed, sigma)
eval_func = nn_network.network_eval('./model.pt')
mtr.set_eval_func(eval_func.eval_model)

print(model_params['cx'])
print(model_params['cy'])
print(model_params['r'])
#mtr.run_metro_conj()
#mtr.metro_alg()
mean = mtr.metro_multi_seed(1)

x = np.zeros(3)
x[0] = model_params['cx']
x[1] = model_params['cy']
x[2] = model_params['r']

mean = np.reshape(mean,[-1])
x_pred = mean[0:3]

np.savez('results.npz',x=x,x_pred=x_pred)

#mean = mtr.compute_mean()
#print(mean)
#print(mtr.compute_var())

#np.savez('results.npz',y_observed=y_observed,y=y,y_pred=y_pred, x=x, x_pred=x_pred)

#mean_vec = np.zeros((1,8))
#var_vec = np.zeros((1,8))
#
#for i in range(0,1):
#    print('iteration :', (i+1))
#    mtr.metro_alg()
#    mean = mtr.compute_mean()
#    var = mtr.compute_var()
#    mean_vec[i,:] = mean
#    var_vec[i,:] = var
#
#
#file_name = 'result.npz'
#np.savez(file_name,params=model_params['blob_params'], mean_vec=mean_vec, var_vec=var_vec)
#
#print(mean)
#print(var)

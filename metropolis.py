import numpy as np
import high_fidelity as hf
import progressbar
import torch

class Metropolis:
	def __init__(self, y_observed, sigma):
		self.y_observed = y_observed
		self.sigma2 = sigma*sigma
		self.epsilon = 2.22044604925e-16
		self.N = 100000

		self.hf = hf.high_fidelity(num_out=10, num_modes=50)
		self.x0 = self.find_initial_state()
		self.X = []

	def find_initial_state(self):
		c1 = np.random.uniform(-0.5,0.5,2)
		r1 = np.random.uniform(0.1,0.5)

		x = np.zeros(3)
		x[0] = c1[0]
		x[1] = c1[1]
		x[2] = r1
		#x[3:103] = np.random.normal(0,1,100)
		return x

	#update conjugation
	def conjugate_initial_state(self,x):
		y = np.zeros(3)
		y[0] = -x[0]
		y[1] = -x[1]
		y[2] = 0.55 - x[2]
		y[3] =  10 - x[3]
		return y

	def set_eval_func(self, eval_func):
		self.eval_func = eval_func


	def density_ratio(self, x, y):
		#modes = np.linspace(1,9,9)
		fx = []
		fy = []
		#for mode in modes:
		#	x[-1] = mode
		#	y[-1] = mode
		torch_x = torch.from_numpy( np.reshape(x,[1,-1]) ).float()
		torch_y = torch.from_numpy( np.reshape(y,[1,-1]) ).float()
		#	fx.append( self.eval_func(torch_x) )
		#	fy.append( self.eval_func(torch_y) )
		fx = self.eval_func(torch_x)
		fy = self.eval_func(torch_y)
			#ex = np.exp( - np.inner(fx - self.y_observed, fx - self.y_observed)/2/self.sigma2)
			#ey = np.exp( - np.inner(fy - self.y_observed, fy - self.y_observed)/2/self.sigma2)
		#fx = np.reshape(fx,[-1])
		#fy = np.reshape(fy,[-1])

		exponent = - np.inner(fy - self.y_observed, fy - self.y_observed) + np.inner(fx - self.y_observed, fx - self.y_observed)

		if(exponent/2/self.sigma2 > 1):
			ratio = 2.0
		else:
			ratio = np.exp(exponent/2/self.sigma2)
		return np.min([1.0, ratio])

	def checker(self, x):
		check = True
		if(x[0] < -0.5 or x[0] > 0.5):
			check = False
		if(x[1] < -0.5 or x[1] > 0.5):
			check = False
		if(x[2] < 0.1 or x[2] > 0.5):
			check = False
		return check

	def run_metro_conj(self):
		self.metro_alg()
		self.x0 = self.conjugate_initial_state(self.x0)
		print(self.compute_mean())
		self.metro_alg()

	def metro_multi_seed(self,num_seeds):
		mean = []
		var = []
		for i in range(num_seeds):
			print('iteration {} out of {}'.format(i+1,num_seeds))
			self.x0 = self.find_initial_state()
			self.metro_alg()
			mean.append( self.compute_mean() )
			var.append( self.compute_var() )
			print(self.compute_mean())
			print(self.compute_var())
			self.X.clear()

		mean = np.reshape(mean,[num_seeds,-1])
		print( np.sum(mean,axis=0)/num_seeds )
		return mean


	def metro_alg(self):
		x = self.x0
		self.X.append(x)

#		for i in range(0,self.N):
		for i in progressbar.progressbar(range(0, self.N)):
			y = np.random.uniform(0,1,3)
			y[0:3] = x[0:3] + 0.1*( y[0:3] - 0.5 )
			if( self.checker(y) ):
				alpha = self.density_ratio(x, y)
				u = np.random.uniform(0,1)
				if( u < alpha ):
					x = y
					self.X.append(y)
				else:
					self.X.append(x)
			else:
				continue

	def compute_mean(self):
		N = len(self.X)
		samples = np.reshape( self.X, [N,-1] )
		mean = np.sum(samples,axis = 0)/N
		return mean

	def compute_var(self):
		N = len(self.X)
		mean = self.compute_mean()
		samples = np.reshape( self.X, [N,-1] )
		samples = samples - mean
		np.set_printoptions(threshold=np.inf)
		cov = np.matmul( samples.T, samples ) / N

		return np.sqrt(np.diagonal(cov))

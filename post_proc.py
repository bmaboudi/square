import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as inter
from matplotlib.pyplot import figure
import matplotlib.patches as patches

class plotter:
    def __init__(self):
        #self.fig, self.ax = plt.subplots(1)
        self.fig = figure(figsize=(8, 8), dpi=80, facecolor='w', edgecolor='k')
        self.ax = self.fig.add_subplot(1, 1, 1)

    def plot_interpolation(self,x,y,color_code,label=None):
        coor_x = np.zeros(5)
        coor_x[0:4] = x
        coor_x[4] = coor_x[0]

        coor_y = np.zeros(5)
        coor_y[0:4] = y
        coor_y[4] = coor_y[0]

        t = np.linspace(0,4,5)
        fx = inter.splrep(t, coor_x,per=True)
        fy = inter.splrep(t, coor_y,per=True)
        t_new = np.linspace(0,4,1000)
        t_new = np.append(t_new, [t_new[0]])

        poly =[]
        poly_x = inter.splev(t_new, fx)
        poly.append(poly_x)
        poly_y = inter.splev(t_new, fy)
        poly.append(poly_y)

        #if( label == None ):
        #    plt.plot(poly_x, poly_y,color_code)
        #else:
        plt.plot(poly_x, poly_y,color_code, label=label)

    def add_points(self,x,y):
        for i in range(0,x.size):
            plt.plot(x[i], y[i],'ko')

    def add_box(self,coor,w,l,label=None,color='r'):
        rect = patches.Rectangle(coor,w,l,linewidth=1,edgecolor=color,facecolor='none',label=label)
        self.ax.add_patch(rect)

    def add_circle(self,coor,r,label=None,color='k'):
    	circ = patches.Circle(coor,r,linewidth=1,edgecolor=color,facecolor='none',label=label)
    	self.ax.add_patch(circ)

    def add_line(self,x,y,color_code,label=None):
        plt.plot(x,y,color_code,label=label)

    def add_label(self,xlabel,ylabel,fontsize=12):
        plt.xlabel(xlabel,fontsize=fontsize)
        plt.ylabel(ylabel,fontsize=fontsize)

    def add_legend(self,fontsize=12):
        plt.legend(fontsize=fontsize)

    def set_plot_lims(self,xlim):
        coors = np.linspace(xlim[0],xlim[1],6)
        self.ax.set_xticks(coors)
        self.ax.set_yticks(coors)

    def plot(self,x,y,color_code):
        plt.plot(x,y,color_code)

    def save_eps(self,path):
        plt.savefig(path, format='eps', dpi=300)

    def show(self):
        plt.show()

def plot_interpolation():
    filename = './data/result_concave.npz'
    data = np.load(filename)
    params = data['params']
    mean = np.reshape( data['mean_vec'] , [8] )

    p = plotter()
    p.add_box([0.35,0.1],0.3,0.35,label='Stochastic Domain')
    p.add_box([0.7,0.25],0.2,0.5)
    p.add_box([0.35,0.55],0.3,0.35)
    p.add_box([0.1,0.25],0.2,0.5)


    p.plot_interpolation(params[0:4],params[4:8],'k-',label='Interpolated Curve')
    #p.plot_interpolation(mean[0:4],mean[4:8],'b-',label='Prediction')
    p.add_points(params[0:4],params[4:8])

    p.add_legend(fontsize=20)
    p.add_label('x','y',fontsize=20)
    p.set_plot_lims([0,1])
    p.save_eps('inter.eps')


def plot_blob_prediction():
    #filename = './data/result_concave3.npz'
    #filename = './data/result_convex.npz'
    filename = './data/result_blob.npz'
    data = np.load(filename)
    params = data['params']
    mean = np.reshape( data['mean_vec'] , [8] )

    p = plotter()
    p.plot_interpolation(params[0:4],params[4:8],'k-',label='exact line')
    p.plot_interpolation(mean[0:4],mean[4:8],'b-',label='estimated line')
    #p.add_points(params[0:4],params[4:8])

    p.add_legend(fontsize=20)
    p.add_label('x','y',fontsize=20)
    p.set_plot_lims([0,1])
    p.save_eps('blob_prediction6.eps')
    #p.show()

def plot_line_prediction():
    #filename = './data/result_1line.npz'
    #filename = './data/result_1line2.npz'
    filename = './data/result_1line3.npz'
    data = np.load(filename)
    params = data['params']
    gaus_mean = data['gaus_mean']
    mean = np.reshape( data['mean_vec'] , [4] )

    p = plotter()
    p.add_box([0,0],1,1,color='k')
    p.add_line([0,0],[0.1,0.9],'r-',label='Stochastic Domain')
    p.add_line([1,1],[0.1,0.9],'r-')

    x=[0,1]
    p.add_line(x,params,'g-o',label='Truth Solution')
    p.add_line(x,mean[2:4],'b-o',label='Prediction')

    p.add_legend(fontsize=20)
    p.add_label('x','y',fontsize=20)
    p.set_plot_lims([-0.2,1.2])
    p.show()
    #p.save_eps('1line_prediction3.eps')

def compute_junction(y1,y2,yp1,yp2):
    a1 = y1-y2
    b1 = -y1
    a2 = yp1-yp2
    b2 = -yp1
    x = -(b1-b2)/(a1-a2)
    y = a1*(b1-b2)/(a1-a2) - b1

    return x,y

def plot_two_line_prediction():
    filename = './data/result_two_line6.npz'
    data = np.load(filename)
    params = data['params']
    mean = data['mean_vec']
    mean = np.sum(mean, axis=0)/mean.shape[0]
    mean = np.reshape( mean[3:7] , [4] )

    p = plotter()
    p.add_box([0,0],1,1,color='k')
    p.add_line([0,0],[0.1,0.9],'r-',label='Stochastic Domain')
    p.add_line([1,1],[0.1,0.9],'r-')

    x=[0,1]
    p.add_line(x,params[0:2],'k-o',label='Truth Solution')
    xend, yend = compute_junction(params[0],params[1],params[2],params[3])
    p.add_line([0,xend],[params[2],yend],'k-o')

    p.add_line(x,mean[0:2],'b-o',label='MCMC estimation')
    xend, yend = compute_junction(mean[0],mean[1],mean[2],mean[3])
    p.add_line([0,xend],[mean[2],yend],'b-o')

    p.add_legend()
    p.add_label('x','y')
    #p.set_plot_lims([0,1])
    #p.show()
    p.save_eps('2line_prediction6.eps')

def plot_circle():
    res_data = np.load('results.npz')
    x = res_data['x']
    x_pred = res_data['x_pred']
    p = plotter()
    p.add_box([-1,-1],2,2,color='k')
    p.add_circle([x[0],x[1]],x[2],color='r',label='Exact')
    p.add_circle([x_pred[0],x_pred[1]],x_pred[2],color='b',label='Predicted')
    p.set_plot_lims([-1.2,1.2])
    p.add_legend()
    p.show()


if __name__=='__main__':
    #plot_interpolation()
    #plot_blob_prediction()
    #plot_line_prediction()
    #plot_two_line_prediction()
	plot_circle()